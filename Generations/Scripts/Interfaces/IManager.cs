namespace Generations.Scripts.Interfaces
{
    public interface IManager
    {
        void ConnectToParentManager();
    }
}