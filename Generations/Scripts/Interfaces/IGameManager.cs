using Generations.Scripts.Main.Game;
using Generations.Scripts.Main.Game.World;

namespace Generations.Scripts.Interfaces
{
    public interface IGameManager
    {
        InputManager InputManager { get; }
        TurnManager TurnManager { get; }
        WorldManager WorldManager { get; }
    }
}