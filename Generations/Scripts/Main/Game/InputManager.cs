using Godot;

namespace Generations.Scripts.Main.Game
{
    public class InputManager: Node2D
    {
        public bool MouseInputDisabled = false;
        
        [Signal]
        private delegate void LeftMouseInputEvent();

        [Signal]
        private delegate void RightMouseInputEvent();
        
        
        public override void  _UnhandledInput(InputEvent @event)
        {
            if (MouseInputDisabled) return;
            
            if (@event is InputEventMouseButton eventMouseButton)
            {
                // Handle left mouse button event
                if (eventMouseButton.ButtonIndex == 1)
                {
                    EmitSignal(nameof(LeftMouseInputEvent), eventMouseButton.Pressed);
                }
                
                // Handle right mouse button event
                if (eventMouseButton.ButtonIndex == 2 && eventMouseButton.Pressed)
                {
                    EmitSignal(nameof(RightMouseInputEvent));
                }
               
            }
        }
    }
}