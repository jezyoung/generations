using Generations.Scripts.Main.Game.World.Characters;
using Generations.Scripts.Main.Game.World.TileMaps;
using Generations.Scripts.Main.Game.World.Tiles;
using Godot;

namespace Generations.Scripts.Main.Game.World
{
    public class WorldManager : Node2D
    {
        [Export] private NodePath _charactersNodePath, _tileMapsNodePath, _tilesNodePath;
        private Node2D _characters;
        private TileMapManager _tileMapManager;
        private TileManager _tileManager;

        public Vector2 MouseTilePosition;
        

        public override void _Ready()
        {
            _characters = GetNode<Node2D>(_charactersNodePath);
            _tileMapManager = GetNode<Node2D>(_tileMapsNodePath) as TileMapManager;
            _tileManager = GetNode<Node2D>(_tilesNodePath) as TileManager;
        }

        public void MoveCharacters()
        {
            // GD.Print("Move Characters");
            // Player player = GetChild(0) as Player;
            // player.MoveTween();

            foreach (var character in _characters.GetChildren())
            {
                // change into 'character' base class
                Player player = character as Player;
                player.MoveTween(MouseTilePosition);
            }
        }

        public string GetSelectedTileName()
        {
           return _tileMapManager.GetSelectedTileName();
        }

        public Vector2 SetMouseTilePosition()
        {
            MouseTilePosition = _tileMapManager.GetNextPosition();
            return MouseTilePosition;
        }
        
        
    }
}