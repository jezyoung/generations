using System.Collections.Generic;
using Generations.Scripts.Main.Game.World.Actions;
using Godot;
using Godot.Collections;

namespace Generations.Scripts.Main.Game.World.Tiles
{
    public class GroundTile: Node
    {
        public Action MoveAction;
        public Action GatherAction;

       public override void _Ready()
       {
           MoveAction = new MoveAction();
           GatherAction = new GatherAction();
           GD.Print(MoveAction.ActionName);
           GD.Print(GatherAction.ActionName);
       }
    }
}