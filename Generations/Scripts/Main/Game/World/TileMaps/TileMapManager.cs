
using Godot;

namespace Generations.Scripts.Main.Game.World.TileMaps
{
    public class TileMapManager: Node2D
    {
        [Export] private NodePath _navigationNodePath, _groundTypesNodePath;
        [Export()] private TileSet _groundTypeTileSet;
        
        

        private TileMap _navigation, _groundTypes;
        private Vector2 _tileMousePosition;

        public override void _Ready()
        {
            _navigation = GetNode<TileMap>(_navigationNodePath);
            _groundTypes = GetNode<TileMap>(_groundTypesNodePath);
        }

        public Vector2 GetNextPosition()
        {
            Vector2 nextPos = _navigation.MapToWorld(_navigation.WorldToMap(GetGlobalMousePosition())) + new Vector2(8,8);
            GD.Print("The next position is: " + nextPos);
            return nextPos;
            // _tileMousePosition = _navigation.WorldToMap(GetGlobalMousePosition());
            // GD.Print(_tileMousePosition);
            // Vector2 pos = _navigation.MapToWorld(_tileMousePosition) + new Vector2(8, 8);
            // GD.Print(pos);
            // return pos;
        }

        public string GetSelectedTileName()
        {
            Vector2 mousePosition = _navigation.WorldToMap(GetGlobalMousePosition());
            string tileName = _groundTypeTileSet.TileGetName(_groundTypes.GetCellv(mousePosition) );
            return tileName;
        }

 
    }
}