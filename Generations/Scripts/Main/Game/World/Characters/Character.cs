using Godot;

namespace Generations.Scripts.Characters
{
    public class Character: Node2D
    {
        public string CharacterName { get; set; }
        public int Age { get; set; }
        public bool IsPlayer { get; set; }
        public bool IsFamily { get; set; }
        public bool IsFemale { get; set; }
        
        CharacterGenerator _characterGenerator = new CharacterGenerator();

        public override void _Ready()
        {
            
        }


        public async void GenerateCharacter()
        {
            Timer t = new Timer();
            AddChild(t);
            t.Start(0.1f);
            await ToSignal(t, "timeout");

            CharacterName = _characterGenerator.NameGenerator();
            Age = _characterGenerator.AgeGenerator(false);
            IsFemale = _characterGenerator.SexGenerator();
            
            // GD.Print(CharacterName + " is " + Age + " year.");
            // GD.Print(IsFemale);
        }
    }
}