using System.Collections.Generic;
using Godot;

namespace Generations.Scripts.Characters
{
    public class CharacterNameData: Node
    {
        public static List<string> FirstPart = new List<string>();
        public static List<string> SecondPart = new List<string>();

        public override void _Ready()
        {
            FirstPart.Add("Geo");
            FirstPart.Add("Fra");
            FirstPart.Add("Bris");
            FirstPart.Add("Jam");
            FirstPart.Add("Tho");
            FirstPart.Add("Wil");
            FirstPart.Add("Cha");
            FirstPart.Add("Mart");
            FirstPart.Add("Ki");
            FirstPart.Add("Bru");
            FirstPart.Add("Sa");
            FirstPart.Add("Gro");
            FirstPart.Add("Plee");
            FirstPart.Add("Hoa");
            FirstPart.Add("Kloi");
            
            SecondPart.Add("ers");
            SecondPart.Add("an");
            SecondPart.Add("ord");
            SecondPart.Add("p");
            SecondPart.Add("ff");
            SecondPart.Add("s");
            SecondPart.Add("nor");
            SecondPart.Add("b");
            SecondPart.Add("grier");
            SecondPart.Add("tian");
            SecondPart.Add("more");
            SecondPart.Add("ders");
            SecondPart.Add("dra");
            SecondPart.Add("noah");
            SecondPart.Add("t");
        }
    }
}