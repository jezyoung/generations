using System.Collections.Generic;
using Godot;

namespace Generations.Scripts.Characters
{
    public class CharacterManager: Node
    {
        public List<Character> Characters = new List<Character>();

        public override void _Ready()
        {
            for (int i = 0; i < 10; i++)
            {
                Characters.Add(new Character());
                Characters[i].GenerateCharacter();
                GD.Print(Characters[i].CharacterName);
            }
        }
    }
}