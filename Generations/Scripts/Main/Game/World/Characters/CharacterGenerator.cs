using Godot;

namespace Generations.Scripts.Characters
{
    public class CharacterGenerator: Node
    {
        RandomNumberGenerator rndmGenerator = new RandomNumberGenerator();
        // CharacterNameData characterNameData = new CharacterNameData();
        
        public override void _Ready()
        {
            
        }

        public int AgeGenerator(bool newBorn)
        {
            rndmGenerator.Randomize();
            return newBorn ? 0 : rndmGenerator.RandiRange(0, 65);
        }

        public string NameGenerator()
        {
            rndmGenerator.Randomize();
            return CharacterNameData.FirstPart[rndmGenerator.RandiRange(0, CharacterNameData.FirstPart.Count)] +
                   CharacterNameData.SecondPart[rndmGenerator.RandiRange(0, CharacterNameData.SecondPart.Count)];

        }

        public bool SexGenerator()
        {
            rndmGenerator.Randomize();
            if (rndmGenerator.RandiRange(0, 1) == 0) return false;
            else return true;
        }
    }
}