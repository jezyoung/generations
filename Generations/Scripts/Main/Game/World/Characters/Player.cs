using Godot;

namespace Generations.Scripts.Main.Game.World.Characters
{
    public class Player: AnimatedSprite
    {
        private Tween _tween = new Tween();

        public override void _Ready()
        {
            AddChild(_tween);
        }

        public void MoveTween(Vector2 nextPos)
        {
            if (Position.DistanceTo(nextPos) > 24) return; // not the right place or script to check this
            _tween.InterpolateProperty(this, "position", Position, nextPos, 0.2f, Tween.TransitionType.Expo,
                Tween.EaseType.In);
            _tween.Start();
        }
    }
}