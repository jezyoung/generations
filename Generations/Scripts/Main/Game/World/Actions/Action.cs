using Godot;

namespace Generations.Scripts.Main.Game.World.Actions
{
    public abstract class Action: Node
    {
        protected string _actionName;
        protected int _turnAmount;

        public string ActionName => _actionName;

        public int TurnAmount => _turnAmount;
        public bool Locked { get; set; }
    }
}