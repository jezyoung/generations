namespace Generations.Scripts.Main.Game.World.Actions
{
    public class GatherAction: Action
    {
        public override void _Ready()
        {
            _actionName = "Gather";
            _turnAmount = 1;
            Locked = false;
        }
    }
}