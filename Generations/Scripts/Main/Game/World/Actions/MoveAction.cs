
namespace Generations.Scripts.Main.Game.World.Actions
{
    public class MoveAction: Action
    {
        public override void _Ready()
        {
            _actionName = "Move";
            _turnAmount = 1;
            Locked = false;
        }
    }
}