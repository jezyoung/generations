using Godot;

namespace Generations.Scripts.Main.Game
{
    public class TurnManager: Node2D
    {
        [Signal]
        private delegate void MoveCharacters();

        public override void _Ready()
        {
            base._Ready();
        }

        public void StartTurnRoutine()
        {
            // GD.Print("Start TurnRoutine");
            // Turn routine logic
            // temp move player:
            EmitSignal(nameof(MoveCharacters));
        }
    }
}