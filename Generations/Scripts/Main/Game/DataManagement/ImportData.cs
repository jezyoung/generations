using Godot;
using Godot.Collections;

namespace Generations.Scripts.Main.Game.DataManagement
{
    public class ImportData: Node
    {
        public Dictionary TileData;
        
        public override void _Ready()
        {
            File tileDataFile = new File();
            tileDataFile.Open("res://Data/TileActionTable.json", File.ModeFlags.Read);
            var tileDataJson = JSON.Parse(tileDataFile.GetAsText());
            tileDataFile.Close();
            TileData = tileDataJson.Result as Dictionary;
            // GD.Print(TileData[0.ToString()]);
        }
    }
}