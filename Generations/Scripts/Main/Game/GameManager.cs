using Generations.Scripts.Main.Game.GUI;
using Generations.Scripts.Main.Game.World;
using Generations.Scripts.Main.Game.World.Tiles;
using Godot;

namespace Generations.Scripts.Main.Game
{
    public class GameManager : Node2D
    {
        public enum Actions
        {
            MOVE, FIGHT, HARVEST, PLANT
        }

        public Actions Action;
        
        [Export] private NodePath _inputManagerPath, _turnManagerPath, _worldManagerPath, _mouseInputPopupsPath;
        private InputManager _inputManager;
        private TurnManager _turnManager;
        private WorldManager _worldManager;
        private MouseInputPopups _mouseInputPopups;

        public override void _Ready()
        {
            _inputManager = GetNode<Node2D>(_inputManagerPath) as InputManager;
            _turnManager = GetNode<Node2D>(_turnManagerPath) as TurnManager;
            _worldManager = GetNode<Node2D>(_worldManagerPath) as WorldManager;
            _mouseInputPopups = GetNode<Control>(_mouseInputPopupsPath) as MouseInputPopups;

            //signal in
            _inputManager.Connect("LeftMouseInputEvent", this, nameof(OnLeftMouseInputEvent));
            _inputManager.Connect("RightMouseInputEvent", this, nameof(OnRightMouseInputEvent));
            _turnManager.Connect("MoveCharacters", this, nameof(OnMoveCharacters));
            _mouseInputPopups.Connect("ActionConfirmed", this, nameof(OnConfirmAction));
            
        }
        
        public void OnLeftMouseInputEvent(bool isPressed)
        {
            if (isPressed)
            {    
                _mouseInputPopups.ShowTileActionPopupMenu(_worldManager.GetSelectedTileName(), _worldManager.SetMouseTilePosition());
            }
            
        }

        public void OnRightMouseInputEvent()
        {
            _mouseInputPopups.HideTileActionPopupMenu();
        }

        void OnConfirmAction(string tileAction)
        {
            GD.Print("On Confirm Action");
            // _turnManager.StartTurnRoutine();
            switch (tileAction)
            {
                case "Move":
                    _worldManager.MoveCharacters();
                    break;
            }
            // start turn coroutine
            _mouseInputPopups.HideTileActionPopupMenu();
        }

        void OnMoveCharacters()
        {
            // _worldManager.MoveCharacters();
        }

        
    }
}