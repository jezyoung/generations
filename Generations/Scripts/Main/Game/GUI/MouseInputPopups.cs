using Godot;

namespace Generations.Scripts.Main.Game.GUI
{
    public class MouseInputPopups: Control
    {
        [Export()] private NodePath _tileInfoLabelNodePath, _tileActionPopupNodePath, _moveButtonNodePath;

        private Label _tileInfoLabel;
        private Popup _tileActionPopup;
        private Button _moveButton;

        [Signal]
        private delegate void ActionConfirmed();

        public override void _Ready()
        {
            _tileInfoLabel = GetNode<Label>(_tileInfoLabelNodePath);
            _tileActionPopup = GetNode<Popup>(_tileActionPopupNodePath);
            _moveButton = GetNode<Button>(_moveButtonNodePath);
        }
        

        public void ShowTileActionPopupMenu(string infoText, Vector2 popPos)
        {
            _tileActionPopup.Popup_();
            _tileActionPopup.SetPosition(popPos);
            _tileInfoLabel.Text = infoText;
        }

        public void HideTileActionPopupMenu()
        {
            _tileActionPopup.Hide();
        }

        void _on_MoveButton_pressed()
        {
            EmitSignal(nameof(ActionConfirmed), "Move");
        }
    }
}