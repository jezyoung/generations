using Godot;

namespace Generations.Scripts.Main
{
    public class MainMenu: Node2D
    {
        private MainManager _mainManager;
        
        public override void _Ready()
        {
            _mainManager = GetParent() as MainManager;
            if(_mainManager == null) GD.PushWarning("MainManger not found!");
        }
    }
}